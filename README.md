# **Sensibus Passthrough**

## Description

This firmware is an implementation of the Sensibus communication protocol on ESP8266 or ESP32 microcontroller,
which allows correct communication between the PC and the SENSIPLUS chips.

---

The firmware is pre-configured for different use cases or modes of communication to the Host or SENSIPLUS.
For example, the SBRMT_USB branch is configured to use the SENSIBUS RMT driver to the SENSIPLUS and the USB serial driver to the Host.
To download this project configuration, simply use the command:

	git clone [Main Repository URL] --recurse-submodules -b SBRMT_USB

The SBRMT_WIFISTA branch uses the same SENSIBUS RMT driver to the sensors and the WiFi TCP/IP driver in station mode to the Host.

	git clone [Main Repository URL] --recurse-submodules -b SBRMT_WIFISTA	

The SBRMT_WIFIAP branch uses the WiFi TCP/IP driver in Access Point mode to communicate with the Host, while keeping the SENSIBUS RMT driver on the SENSIPLUS.

	git clone [Main Repository URL] --recurse-submodules -b SBRMT_WIFIAP

The SYNC_READ branch contains the driver with the ability to perform buffered synchronous reads, using the SENSIBUS RMT and USB Serial.

	git clone [Main Repository URL] --recurse-submodules -b SYNC_READ

Associated with the main branch is the legacy version, which uses the SENSIBUS bit-banging driver to the SENSIPLUS and the USB serial driver to the Host.

	git clone [Main Repository URL] --recurse-submodules

---

## Setup of an Eclipse-based development environment

### Requirements 

- Eclipse IDE with Arduino Plugin (e.g. [Sloeber](http://eclipse.baeyens.it/) which is used in this guide)
- The right microcontroller toolchain installed: [ESP8266](https://github.com/esp8266/Arduino) or [ESP32](https://github.com/espressif/arduino-esp32)
- If you are on Linux, remember to set permissions to use the serial ports. For example:
	- Debian/Ubuntu:
	
			sudo usermod -a -G dialout $USER
			
	- Arch:
	
			sudo usermod -a -G uucp $USER


### Installation 

- Download repository and its submodules in a temporary folder:
		
		git clone https://gitlab.com/sensichips_collaborators/sensibus_passthrough --recurse-submodules   

1. Create new Arduino project from Eclipse IDE:
	- ![](tutorial/1.png)
2. Use the appropriate name: 
	- ![](tutorial/2.png)
3. Select right config for the board in use:
    - ESP32 example: 
		- ![](tutorial/3_1.png)
    - ESP8266 example: 
		- ![](tutorial/3_2.png)
4. **Finish** the project creation
5. Import source files from repository previously downloaded: 
    - Right mouse click on project name in *Project explorer* and then *Import...* 
    - Select import from File System: 
		- ![](tutorial/4.png)
    - Select the repository folder: 
		- ![](tutorial/5.png)
    - **Finish** the import operation, overwrite files if necessary
6. Build the project

---

## License

© 2024, Sensichips Srl