#ifdef ESP8266                  // ESP8266
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiClient.h>
// Hardcode WiFi parameters as this isn't going to be moving around.
ESP8266WiFiMulti wifiMulti;     // Create an instance of the ESP8266WiFiMulti class, called 'wifiMulti'


#else                           // ESP32
#include <WiFi.h>
#include <WiFiMulti.h>
WiFiMulti wifiMulti;     
#endif

#include "./DRIVER/ESP8266/StatusSENSIBUS.h"
#include "./DRIVER/ESP8266/MsgSENSIBUS.h"

#include "./DRIVER/ESP8266/CommSENSIBUS.h"
#include "./DRIVER/ESP8266/CommUSB.h"

// WIFI is generally incompatible with speed > 1x
// Uncomment this line in order to activate WIFI
//#define ACTIVATE_WIFI
#ifdef ACTIVATE_WIFI
#include "./DRIVER/ESP8266/CommTCP.h"
// Start a TCP Server on port 8999
WiFiServer server(8999);
WiFiClient client;
CommTCP commTCP;
boolean WIFI_AVAILABLE = true;
void printWifiStatus();
#endif


// Simple test:
// 81 -> d4 02
// 41 00 00 00 00 00 00 D8 DC -> DC
// 02 2B -> 10 07 (after SENSIPLUS POR)

// File system examples
// https://github.com/esp8266/arduino-esp8266fs-plugin
// https://storage.thomasdendale.com/ESP8266/Filesystem.pdf
// https://circuits4you.com/2018/01/31/example-of-esp8266-flash-file-system-spiffs/


CommSENSIBUS commSENSIBUS(DATI);
StatusSENSIBUS statusSENSIBUS;
MsgSENSIBUS msgSENSIBUS;
CommUSB commUSB;

void sendEcho();
void initialize();
void activateWiFi();

void setup() {

  initialize();
  Serial.flush();
  commSENSIBUS.tryDetectRUN();

}

void loop() {

   /*
  if (flag){
    Serial.flush ();   // wait for send buffer to empty
    delay (2);    // let last character be sent
    Serial.end ();
    Serial.begin(USB_SPEED[0]);
    flag = false;
  }  */
  
  msgSENSIBUS.reset();

#ifdef ACTIVATE_WIFI
  if (WIFI_AVAILABLE){
    client = server.available();

    if (client && client.connected()){
      Serial.println("Connected ... ");
      commTCP.init(&client, &server);
    }

    // Receive data until connection is live
    while (client && client.connected()){
      commTCP.receiveHeader(msgSENSIBUS, statusSENSIBUS);
      //Serial.println("Received header ...");
      if (msgSENSIBUS.instructionType != NO_INSTR){
        
        // Turn on led
        WRITE_LOW(LED_BUILTIN);
        //digitalWrite(LED_BUILTIN, LOW);
        
        // Receive data from Host via USB
        commTCP.receiveData(msgSENSIBUS, statusSENSIBUS);
        
        // SENSIBUS transaction
        Serial.println("Call transaction on sensibus ... ");
        commSENSIBUS.transaction(msgSENSIBUS, statusSENSIBUS);

        // Turn off led
        WRITE_HIGH(LED_BUILTIN);
        //digitalWrite(LED_BUILTIN, HIGH);

        
        // Send response to Host via USB
        Serial.println("Call sendResponse on TCP");
        commTCP.sendResponse(msgSENSIBUS); 
      }      
    }
  }
#endif


  // Receive data from Host via USB
  // Receive the header. This method modify the statusSENSIBUS object.
  commUSB.receiveHeader(msgSENSIBUS, statusSENSIBUS);

  if (msgSENSIBUS.instructionType != NO_INSTR){
    // Turn on led
    WRITE_LOW(LED_BUILTIN); //           digitalWrite(pin, LOW)
    //digitalWrite(LED_BUILTIN, LOW);
    
    // Receive data from Host via USB
    commUSB.receiveData(msgSENSIBUS, statusSENSIBUS);
    
    // SENSIBUS transaction
    commSENSIBUS.transaction(msgSENSIBUS, statusSENSIBUS);

    // Turn off led
    WRITE_HIGH(LED_BUILTIN);
    //digitalWrite(LED_BUILTIN, HIGH);
    
    
    // Send response to Host via USB
    commUSB.sendResponse(msgSENSIBUS);
    
  }
  
  delay(1);
  
  // Turn off led
  WRITE_HIGH(LED_BUILTIN);
  // digitalWrite(LED_BUILTIN, HIGH);

  yield();
  
  
}

#ifdef ACTIVATE_WIFI
void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void activateWiFi(){

  wifiMulti.addAP("AndroidMM", "XXX");
  wifiMulti.addAP("FPV_WIFI__F994_EXT", "");
  wifiMulti.addAP("APCarmine", "23456789");
  wifiMulti.addAP("XPS_Carmine", "23456789");
  
  int i = 0;
  int counter = 0;
  int maxTrial = 10;  
  
  WIFI_AVAILABLE = wifiMulti.run() == WL_CONNECTED;
  while (!WIFI_AVAILABLE && i < maxTrial) { // Wait for the Wi-Fi to connect
    delay(500);
    Serial.print('.');
    WIFI_AVAILABLE = wifiMulti.run() == WL_CONNECTED;
    i++;
  }

  printWifiStatus();

  if (WIFI_AVAILABLE){
    // Start the TCP server
    server.begin();
  }
  
}

#endif



void initialize(){
    // In order to use D2 => SENSIBUS_VCC as a pin for the connector. Is not used as an active pin.
  pinMode(SENSIBUS_VCC, OUTPUT_OPEN_DRAIN); 
  digitalWrite(SENSIBUS_VCC, HIGH);
  
  pinMode(LED_BUILTIN, OUTPUT); // led

  Serial.begin(USB_SPEED[2]);  // 0 => 115200 default speed, 1 => 230400, 2 => 460800 max tested speed
  Serial.setTimeout(3000);

#ifdef ACTIVATE_WIFI
  activateWiFi();  
#else
  WiFi.mode(WIFI_OFF);
#endif  
  
}


void sendEcho(){
  Serial.write(msgSENSIBUS.chipAddressLength);
  for(int i = 0; i < msgSENSIBUS.chipAddressLength; i++){
    Serial.write(msgSENSIBUS.chipAddress[i]);
  }
  Serial.write(msgSENSIBUS.registerAddress[0]);
  Serial.write(statusSENSIBUS.isRead);
  Serial.write(statusSENSIBUS.addressingMode);
  Serial.write(msgSENSIBUS.dataRcvLength);

}


